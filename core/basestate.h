#ifndef BASESTATE
#define BASESTATE

#include "basicmath.h"
#include "ibase.h"

namespace Nbuk{
    class BaseState
    {
    public:
        iBase* mBase;
        uint mNumberOfNodes;
        uint mBinState;
        uint mNumber; //number on the list of states

        BaseState(iBase* nBase, uint nNumberOfStates, uint nBinState, uint nNumber) :
          mBase{ nBase }
          , mNumberOfNodes{nNumberOfStates}
          , mBinState{nBinState}
          , mNumber{ nNumber }
        {
        }

        BaseState(const BaseState& nState) :
            mBase{ nState.mBase }
          , mNumberOfNodes{nState.mNumberOfNodes}
          , mBinState{nState.mBinState}
          , mNumber{nState.mNumber}
        {

        }

        BaseState& operator = (const BaseState& nState)
        {
            mBase = nState.mBase;
            mNumberOfNodes = nState.mNumberOfNodes;
            mBinState = nState.mBinState;
            mNumber = nState.mNumber;
        }

        std::vector<uint> neighborNodeStates() const
        {
            std::vector<uint> oStates;

            for(uint i=0; i<(mNumberOfNodes-1); i++){
                oStates.push_back( (mBinState >> (mNumberOfNodes-2-i))%4 );
            }

            return oStates;
        }

        uint oneNodeSubstate(uint nN) const
        {
            uint dn = mNumberOfNodes-nN-1;
            uint valueN = (mBinState & (1 << dn));
            return valueN;
        }

        uint twoNodeSubstate(uint nN, uint nM) const
        {
            uint dn = mNumberOfNodes-nN-1;
            uint dm = mNumberOfNodes-nM-1;

            uint valueN = (mBinState & (1 << dn));
            if(dn==0){
                valueN = valueN << 1;
            } else {
                valueN = valueN >> dn-1;
            }
            uint valueM = (mBinState & (1 << dm)) >> dm;

            return valueN | valueM;
        }

        void writeState() const
        {
            output << toBin(mBinState, mNumberOfNodes) << std::endl;
        }

        void writeSubstates()
        {
            output << toBin(mBinState, mNumberOfNodes) << std::endl << std::endl;
            for(auto& num : neighborNodeStates()){
                output << toBin(num, 2) << std::endl;
            }
        }

        void swapNodes(uint nN, uint nM)
        {
            uint dn = mNumberOfNodes-nN-1;
            uint dm = mNumberOfNodes-nM-1;

            uint valueN = (mBinState & (1 << dn)) >> dn;
            uint valueM = (mBinState & (1 << dm)) >> dm;

            mBinState &= ~((1<<dn) | (1<<dm));
            mBinState |= (valueN << dm) | (valueM << dn);

            mNumber = mBase->identifyStateNumber(mBinState);
        }
    };
}

#endif // BASESTATE

