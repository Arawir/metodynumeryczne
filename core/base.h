#ifndef BASE
#define BASE

#include <cassert>
#include "ibase.h"
#include "basestate.h"

namespace Nbuk{

    class Base : public iBase
    {
    public:
        ExperimentConfig& mConfig;
        std::vector<BaseState> mStateVector;

    public:
        Base(ExperimentConfig& nConfig) :
            iBase{ }
          , mConfig{ nConfig }
          , mStateVector{ }
        {

        }

        void generateBase() override
        {
            mStateVector.clear();
            if(mConfig.mMagnetization.mSelector==MagnetizationSelector::All){

            } else {
                gen(0,mConfig.mNumberOfNodes, 0b0, static_cast<int>(mConfig.mMagnetization.mValue)+static_cast<int>(mConfig.mNumberOfNodes)/2);
            }
        }

        uint size() override
        {
            if(mConfig.mMagnetization.mSelector==MagnetizationSelector::All){
                return static_cast<uint>( pow(2, mConfig.mNumberOfNodes) );
            }
            return mStateVector.size();
        }

        uint identifyStateNumber(uint nBinState) override
        {
            if(mConfig.mMagnetization.mSelector==MagnetizationSelector::All){
                return nBinState;
            }
            return stateWithBin(nBinState)->mNumber;
        }

        uint identifyStateBin(uint nNumber) override
        {
            if(mConfig.mMagnetization.mSelector==MagnetizationSelector::All){
                return nNumber;
            }
            return stateWithNumber(nNumber)->mNumber;
        }

        BaseState state(uint n)
        {
            if(mConfig.mMagnetization.mSelector==MagnetizationSelector::All){
                return BaseState{this, mConfig.mNumberOfNodes, n, n};
            }
            return mStateVector.at(n);
        }

        void writeBase() override
        {
            for(auto& state : mStateVector){
                state.writeState();
            }
        }

    private:
        void gen(uint nFirstNode, uint nEndNode, uint nCreatedState, int nMagnetizationToAdd)
        {
            if(nFirstNode==nEndNode){
                mStateVector.push_back( {this, mConfig.mNumberOfNodes, nCreatedState, (uint)mStateVector.size()} );
            } else {
                if((nEndNode-nFirstNode-1)>=nMagnetizationToAdd){
                    gen(nFirstNode+1, nEndNode, (nCreatedState<<1), nMagnetizationToAdd);
                }
                if(nMagnetizationToAdd>0){
                    gen(nFirstNode+1, nEndNode, (nCreatedState<<1)+1, nMagnetizationToAdd-1);
                }
            }
        }

        BaseState* stateWithBin(uint nBin)
        {
            for(auto& state : mStateVector){
                if(state.mBinState==nBin) return &state;
            }
            assert("Cannot find state with required bin!");
            return nullptr;
        }

        BaseState* stateWithNumber(uint nNumber)
        {
            for(auto& state : mStateVector){
                if(state.mNumber==nNumber) return &state;
            }
            assert("Cannot find state with required number!");
            return nullptr;
        }
    };

}

#endif // BASE

