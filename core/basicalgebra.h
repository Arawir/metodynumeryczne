#ifndef BASICALGEBRA
#define BASICALGEBRA

#include "state.h"

namespace Nbuk{
//    arma::cx_vec operator *(arma::cx_vec &vec, State &state);
//    arma::cx_vec operator *(State &state, arma::cx_vec &vec);
//    arma::cx_vec operator *(State &state1, State &state2);
//    arma::cx_vec operator *(cpl &val, State &state);
//    arma::cx_vec operator *(State &state, cpl &val);

   // arma::cx_mat operator *(arma::cx_mat &mat, State &state);
    //arma::cx_mat operator *(State &state, arma::cx_mat &mat);
    arma::cx_mat operator *(Operator &oper, State &state);
    //arma::cx_mat operator *(State &state, Operator &oper);
    arma::cx_mat operator *(arma::cx_rowvec vec, Operator &oper);
    cpl operator *(arma::cx_rowvec vec, State &state);


//    arma::cx_mat operator *(cpl &val, Operator &oper);
//    arma::cx_mat operator *(Operator &oper, cpl &val);

//    arma::cx_mat operator *(Operator &oper1, Operator &oper2);
//    arma::cx_mat operator *(Operator &oper, arma::cx_mat &mat);
//    arma::cx_mat operator *(arma::cx_mat &mat, Operator &oper);

}

#endif // BASICALGEBRA

