#ifndef TESTS
#define TESTS

#include "nbuk.h"

void lanczosTest2(){
    arma::cx_mat A = arma::randu<arma::cx_mat>(1000,1000);
    A += A.t();

    arma::cx_vec startVec = randomVector((uint)sqrt(A.size()));

    auto lanczosEigens = lanczos(A,startVec,50);
    arma::cx_vec lanczosGS = eigenstateLanczos(A, startVec, lanczosEigens[0].mVector, 50);
   // arma::cx_vec exactGS = exactDiagonalization(A)[0].mVector;


    output << "E exact: " << exactDiagonalization(A)[0].mValue << std::endl;
    output << "E lanczos: " << lanczosEigens[0].mValue << std::endl;
    output << "<lGS|A|lGS> = " << lanczosGS.t()*A*lanczosGS << std::endl;

//    output << exactGS << std::endl;
//    output << lanczosGS << std::endl;
//    output << lanczosGS - exactGS << std::endl;
}

void lanczosTest(){
    arma::cx_mat A = arma::randu<arma::cx_mat>(10,10);
    A += A.t();

    for(auto& eigen : lanczos(A, 70)){
        output << eigen.mValue << std::endl;
    }
    output << std::endl;
    for(auto& eigen : exactDiagonalization(A)){
        output << eigen.mValue << std::endl;
    }
}

void datasetTest()
{
    Dataset dataset;
    dataset.addParam("a", {1,2,3});
    dataset.addParam("b", -1.0, 1.0, 7);
    dataset.dataname() = "data";
    dataset.sort({"b", "a"});
    dataset.generateDatabaseAndGoToFirstField();

    for(auto& a : dataset.vectorOf("a")){
        output << a << std::endl;
    }

    output << dataset;
}

#endif // TESTS

