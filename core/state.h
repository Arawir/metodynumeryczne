#ifndef STATE
#define STATE

#include "operator.h"

namespace Nbuk{
    class State
    {
    public:
        arma::cx_vec mVector;
        Base& mBase;
    public:
        State(Base& nBase) :
            mVector{}
          , mBase{ nBase }
        {

        }

        ~State() = default;

        void setToBin( uint nBaseStateBin )
        {
            prepareBase();
            uint lPosition = mBase.identifyStateNumber(nBaseStateBin);
            mVector[lPosition] = 1.0;
        }

        arma::cx_vec atTime(double nTime, Operator& nHamiltonian)
        {
            arma::cx_vec lVector{ mVector.size(), arma::fill::zeros};

            for(auto& eigen : nHamiltonian.mEigens){
                output << as_scalar(eigen.mVector.t()*eigen.mVector) << std::endl;
                lVector += eigen.mVector*exp(-im*nTime*eigen.mValue)*as_scalar(eigen.mVector.t()*mVector);
            }
            return lVector;
        }

        void timeEvolution(double nDeltaT, Operator& nHamiltonian)
        {
            arma::cx_vec lVector{ mVector.size(), arma::fill::zeros};

            for(auto& eigen : nHamiltonian.mEigens){
                lVector += eigen.mVector*exp(-im*nDeltaT*eigen.mValue)*as_scalar(eigen.mVector.t()*mVector);
            }
            mVector = lVector;
            normalize();
        }

        arma::cx_rowvec t()
        {
            return mVector.t();
        }

        arma::cx_vec &vector()
        {
            return mVector;
        }

        operator arma::cx_vec()
        {
            return mVector;
        }

        void operator =(arma::cx_vec vec)
        {
            assert(vec.size()==mBase.size());
            mVector = vec;
        }


    private:
        void prepareBase()
        {
            if(mVector.size() != mBase.size()){
                mVector.resize(mBase.size());
            }
            mVector.zeros();
        }
        void normalize()
        {
            assert(as_scalar(mVector.t()*mVector).imag()==0.0);
            double lNorm = sqrt( as_scalar(mVector.t()*mVector).real() );
            for(auto& stateValue : mVector){
                stateValue /= lNorm;
            }
        }
    };
}

#endif // STATE

