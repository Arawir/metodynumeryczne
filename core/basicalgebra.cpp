#include "basicalgebra.h"

using namespace arma;

namespace Nbuk{
    arma::cx_vec operator *(arma::cx_vec &vec, State &state)
    {
        return vec*state.vector();
    }

    arma::cx_vec operator *(State &state, arma::cx_vec &vec)
    {
        return state.vector()*vec;
    }

    arma::cx_vec operator *(State &state1, State &state2)
    {
        return state1.vector()*state2.vector();
    }

    arma::cx_vec operator *(cpl &val, State &state)
    {
        return val*state.vector();
    }

    arma::cx_vec operator *(State &state, cpl &val)
    {
        return state.vector()*val;
    }

    arma::cx_mat operator *(arma::cx_mat &mat, State &state)
    {
        return mat*state.vector();
    }

    arma::cx_mat operator *(State &state, arma::cx_mat &mat)
    {
        return state.vector()*mat;
    }

    arma::cx_mat operator *(Operator &oper, State &state)
    {
        return oper.matrix()*state.vector();
    }

    arma::cx_mat operator *(State &state, Operator &oper)
    {
        return state.vector()*oper.matrix();
    }

    arma::cx_mat operator *(arma::cx_rowvec vec, Operator &oper)
    {
        return vec*oper.matrix();
    }

    cpl operator *(arma::cx_rowvec vec, State &state)
    {
        return arma::as_scalar( arma::operator *(vec,state.vector()));
    }

    arma::cx_mat operator *(cpl &val, Operator &oper)
    {
        return val*oper.matrix();
    }

    arma::cx_mat operator *(Operator &oper, cpl &val)
    {
        return oper.matrix()*val;
    }

    arma::cx_mat operator *(Operator &oper1, Operator &oper2)
    {
        return oper1.matrix()*oper2.matrix();
    }

    arma::cx_mat operator *(Operator &oper, arma::cx_mat &mat)
    {
        return oper.matrix()*mat;
    }

    arma::cx_mat operator *(arma::cx_mat &mat, Operator &oper)
    {
        return mat*oper.matrix();
    }
}

