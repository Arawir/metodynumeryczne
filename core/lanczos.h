#ifndef LANCZOS_H
#define LANCZOS_H

#include "basicmath.h"

namespace Nbuk{
    arma::cx_vec randomVector(uint nSize);

    arma::cx_vec eigenstateLanczos(const arma::cx_mat &mat,
                                   arma::cx_vec startVector,
                                   arma::cx_vec lanczosVec,
                                   uint M);

    void prepareLanczosVectors(arma::cx_vec &a,
                               arma::cx_vec &b,
                               const arma::cx_mat &mat,
                               arma::cx_vec startVector,
                               uint M);

    std::vector<Eigen> lanczos(const arma::cx_mat &mat,
                               arma::cx_vec startVector,
                               uint M,
                               arma::cx_mat &V);

    std::vector<Eigen> lanczos(const arma::cx_mat &mat,
                               uint M,
                               arma::cx_mat &V);

    std::vector<Eigen> lanczos(const arma::cx_mat &mat,
                               arma::cx_vec startVector,
                               uint M);

    std::vector<Eigen> lanczos(const arma::cx_mat &mat,
                               uint M);
}

#endif // LANCZOS_H

