#ifndef TIMEEVOLUTIONOPERATOR
#define TIMEEVOLUTIONOPERATOR

#include "operator.h"
#include "basichamiltonian.h"
namespace Nbuk{
    class TmeEvOper : public Operator
    {
    public:
        BasicHamiltonian &mH;
        double mDeltaT = 0.0;

    public:
        TmeEvOper(BasicHamiltonian &nH) :
            Operator{ nH.mBase }
          , mH{ nH }
        {

        }

        void regenerate() override
        {
            mMatrix.resize(mH.mBase.size(), mH.mBase.size());
            mMatrix.zeros();
            for(auto& eigen : mH.mEigens){
                mMatrix += ( cos(-mDeltaT*eigen.mValue) + im*sin(-mDeltaT*eigen.mValue) )*eigen.mVector*eigen.mVector.t();
            }
        }
    };
}

#endif // TIMEEVOLUTIONOPERATOR

