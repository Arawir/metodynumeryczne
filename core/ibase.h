#ifndef IBASE
#define IBASE

#include "basicmath.h"
//#include "basestate.h"

namespace Nbuk{
    class iBase
    {
    public:
        iBase()
        {

        }
        virtual ~iBase() = default;

        virtual void generateBase() = 0;
        virtual uint size() = 0;
        virtual uint identifyStateNumber(uint nBinState) = 0;
        virtual uint identifyStateBin(uint nNumber) = 0;
        virtual void writeBase() = 0;
    };
}

#endif // IBASE

