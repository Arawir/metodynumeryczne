#ifndef DATASET
#define DATASET

#include <string>
#include <vector>
#include <cassert>
#include <iostream>
#include <fstream>
#include "basicmath.h"

namespace Nbuk{

    enum class ParamType
    {
        real, intiger, complex
    };

    class iParam
    {
    public:
        iParam() = default;
        virtual ~iParam() = default;
        virtual std::string name() = 0;
        virtual uint size() = 0;
        virtual uint indexWithValue(cpl nValue) = 0;
    };


    class ParamCpl : public iParam
    {
    public:
        std::string mName;
        std::vector<cpl> values;

    public:
        ParamCpl(std::string nName, std::vector<cpl> nValues) :
            mName{ nName }
          , values{ nValues }
        {

        }
        std::string name() override { return mName; }
        uint size() override { return values.size(); }

        uint indexWithValue(cpl nValue) override
        {
            for(uint i=0; i<values.size(); i++)
            {
                if(mabs(values[i]-nValue)<0.0001){ return i; }
            }
            assert(false);
            return 0;
        }
    };


    enum class Verbosity
    {
        quiet, progress
    };

    class Dataset
    {
        std::vector<iParam*> parametersCpl;
        std::vector<cpl> data;
        std::string dataName;
        uint currFieldNum;
        Verbosity mVerbose = Verbosity::quiet;
    public:
        Dataset() = default;
        ~Dataset()
        {
            for(auto& param : parametersCpl){
                delete param;
            }
        }

        auto &vectorOf(std::string nParamName)
        {
            return ((ParamCpl*)param(nParamName))->values;
        }

        auto &vectorOf(iParam* param)
        {
            return ((ParamCpl*)param)->values;
        }

        auto &field( std::vector<cpl> nParamValues)
        {
            assert(nParamValues.size()==parametersCpl.size());
            uint fieldNum = 0;

            for(uint p=0; p<parametersCpl.size(); p++){
                fieldNum += parametersCpl[p]->indexWithValue(nParamValues[p])*sizeAfterIndex(p);
            }
            writeProgressIfNeeded(fieldNum);

            return data[fieldNum];
        }

        auto &fieldI( std::vector<uint> paramIndexes)
        {
            assert(paramIndexes.size()==parametersCpl.size());
            uint fieldNum = 0;
            for(uint p=0; p<parametersCpl.size(); p++){
                fieldNum += paramIndexes[p]*sizeAfterIndex(p);
            }
            writeProgressIfNeeded(fieldNum);

            return data[fieldNum];
        }

        auto &field()
        {
            writeProgressIfNeeded();
            return data[currFieldNum];
        }


        cpl paramInFieldNumber(std::string nName, uint nField)
        {
            return ((ParamCpl*)param(nName))->values[nField];
        }

        cpl paramDelta(std::string nName)
        {
            return ((ParamCpl*)param(nName))->values[1]-((ParamCpl*)param(nName))->values[0]; //WARNING EXPERIMENTAL!!!
        }

        uint numberOfParameters(std::string nName)
        {
            return param(nName)->size();
        }

        cpl paramWithIndex(std::string nParamName, uint nIndex)
        {
            return ((ParamCpl*)param(nParamName))->values[nIndex];
        }

        Verbosity &verbosity()
        {
            return mVerbose;
        }

        std::string &dataname()
        {
            return dataName;
        }

        bool paramWasChanged(std::string nName)
        {
            if(currFieldNum==0){ return true; }
            if(valueOfParamInField(nName, currFieldNum-1) != valueOfParamInField(nName, currFieldNum)){
                return true;
            }
            return false;
        }

        cpl valueOfParam(std::string nName)
        {
            return valueOfParamInField(nName, currFieldNum);
        }

        void addParam(std::string nName, std::vector<cpl> nValues)
        {
            parametersCpl.push_back( new ParamCpl{ nName, nValues} );
        }

        void addParam(std::string nName, cpl minVal, cpl maxVal, uint numOfPoints)
        {
            std::vector<cpl> tValues;
            tValues.resize(numOfPoints);
            cpl dVal = (maxVal-minVal)/(double)(numOfPoints-1);
            for(uint i=0; i<numOfPoints; i++){
                tValues[i] = minVal + (double)i*dVal;
            }

            parametersCpl.push_back( new ParamCpl{ nName, tValues} );
        }

        void generateDatabaseAndGoToFirstField()
        {
            data.resize(size());
            std::fill(data.begin(), data.end(), 0);
            goToFirstField();
        }

        void sort(std::vector<std::string> paramNames)
        {
            std::vector<iParam*> tParameters;

            for(auto& name : paramNames){
                for(auto& param : parametersCpl){
                    if(param->name() == name){ tParameters.push_back(param); }
                }
            }
            parametersCpl = tParameters;
        }

        void goToFirstField()
        {
            currFieldNum = 0;
            writeProgressIfNeeded();
        }

        void goToNextField()
        {
            currFieldNum++;
            writeProgressIfNeeded();
        }

        void goToFieldWithNextValueOf(std::string nName)
        {
            currFieldNum++;
            while(!paramWasChanged(nName)){
                currFieldNum++;
            }
            writeProgressIfNeeded();
        }

        uint currentFieldNumber()
        {
            return currFieldNum;
        }

        void goToFieldWith(uint number)
        {
            currFieldNum = number;
            writeProgressIfNeeded();
        }

        bool fieldExists()
        {
            return currFieldNum < size();
        }

        uint size()
        {
            uint oSize = 1;
            for(auto& param : parametersCpl){
                oSize *= param->size();
            }
            return oSize;
        }
        void saveToFile(std::string fileName)
        {
            saveToFile(fileName, "fixed");
        }

        void saveToFile(std::string fileName, std::string flag)
        {
            if(flag=="scientific"){
                std::fstream file(fileName.c_str(), std::ios::out);
                file << std::scientific << *this;
                file.close();
            } else {
                std::fstream file(fileName.c_str(), std::ios::out);
                file << std::fixed << *this;
                file.close();
            }
        }

        void saveToFiles2D(std::string fileName)
        {
            std::fstream file{"a", std::ios::out};
            goToFirstField();

            while(fieldExists()){
                for(uint i=0; i<parametersCpl.size()-1; i++){
                    if(paramWasChanged(parametersCpl[i])){
                        file.close();
                        std::string name = fileName;
                        for(uint j=0; j<parametersCpl.size()-1; j++){
                            name += parametersCpl[j]->name();
                            name += std::to_string(valueOfParamInField(parametersCpl[j]->name(),currFieldNum).real());
                            name += "_";
                        }

                        file.open(name.c_str(), std::ios::out);

                        file << parametersCpl[parametersCpl.size()-1]->name() << "(real) ";
                        file << parametersCpl[parametersCpl.size()-1]->name() << "(imag) ";
                        file << dataname() << "(real) " << " ";
                        file << dataname() << "(imag) " << std::endl;
                    }
                }
                file << std::fixed << std::setprecision(4)
                     << valueOfParamInField(parametersCpl.back()->name(),currFieldNum).real() << " "
                     << valueOfParamInField(parametersCpl.back()->name(),currFieldNum).imag() << " "
                     << field().real() << " "
                     << field().imag() << " "
                     << std::endl;
                currFieldNum++;
            }

            file.close();
        }


        void saveToFiles3D(std::string fileName)
        {
            std::fstream file{"a", std::ios::out};
            goToFirstField();

            while(fieldExists()){
                for(uint i=0; i<parametersCpl.size()-2; i++){
                    if(paramWasChanged(parametersCpl[i])){
                        file.close();
                        std::string name = fileName;
                        for(uint j=0; j<parametersCpl.size()-2; j++){
                            name += parametersCpl[j]->name();
                            name += std::to_string(valueOfParamInField(parametersCpl[j]->name(),currFieldNum).real());
                            name += "_";
                        }

                        file.open(name.c_str(), std::ios::out);

                        file << parametersCpl[parametersCpl.size()-2]->name() << "(real) ";
                        file << parametersCpl[parametersCpl.size()-2]->name() << "(imag) ";
                        file << parametersCpl[parametersCpl.size()-1]->name() << "(real) ";
                        file << parametersCpl[parametersCpl.size()-1]->name() << "(imag) ";
                        file << dataname() << "(real) " << " ";
                        file << dataname() << "(imag) " << std::endl;
                    }
                }
                if(paramWasChanged(parametersCpl[parametersCpl.size()-2]->name()) ){
                    file << std::endl;
                }
                file << std::fixed << std::setprecision(4)
                     << valueOfParamInField(parametersCpl[parametersCpl.size()-2]->name(),currFieldNum).real() << " "
                     << valueOfParamInField(parametersCpl[parametersCpl.size()-2]->name(),currFieldNum).imag() << " "
                     << valueOfParamInField(parametersCpl.back()->name(),currFieldNum).real() << " "
                     << valueOfParamInField(parametersCpl.back()->name(),currFieldNum).imag() << " "
                     << field().real() << " "
                     << field().imag() << " "
                     << std::endl;
                currFieldNum++;
            }

            file.close();
        }

        friend std::ostream& operator<<(std::ostream& os, Dataset& dataset);
    private:
        uint sizeAfterIndex(uint i)
        {
            uint oSize=1;
            for(uint n=i+1; n<parametersCpl.size(); n++){
                oSize *= parametersCpl[n]->size();
            }
            return oSize;
        }

        cpl valueOfParamInField(std::string nName, uint nFieldNumber)
        {
            for(uint i=0; i<parametersCpl.size(); i++){
                if(parametersCpl[i]->name() == nName){
                    uint tIndex = (nFieldNumber/sizeAfterIndex(i)) % parametersCpl[i]->size();
                    return ((ParamCpl*)parametersCpl[i])->values[tIndex];
                }
            }
            assert(false);
            return 0.0;
        }

        void writeProgressIfNeeded()
        {
            if(mVerbose==Verbosity::progress){
                for(auto& param : parametersCpl){
                    output << param->name() << " "
                           << valueOfParam(param->name()) << " | ";
                }
                output << std::endl;
            }
        }
        void writeProgressIfNeeded(uint nFieldNumber)
        {
            if(mVerbose==Verbosity::progress){
                for(auto& param : parametersCpl){
                    output << param->name() << " "
                           << valueOfParamInField(param->name(), nFieldNumber) << " | ";
                }
                output << std::endl;
            }
        }

        iParam* param(std::string nName)
        {
            for(auto& param : parametersCpl){
                if(param->name() == nName){ return param; }
            }
            assert(false);
            return nullptr;
        }

        bool paramWasChanged(iParam* param)
        {
            if(currFieldNum==0){ return true; }
            if(valueOfParamInField(param->name(),currFieldNum-1) != valueOfParamInField(param->name(),currFieldNum)){ //UGLY
                return true;
            }
            return false;
        }
    };

    std::ostream& operator<<(std::ostream& os, Dataset& dataset);
}

#endif // DATASET

