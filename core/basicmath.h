#ifndef BASICMATH
#define BASICMATH

#include <iostream>
#include <iomanip>
#include <armadillo>
#include <complex>

#define output std::cout << std::fixed << std::setprecision(4)
#define im std::complex<double>{0,1}
#define cpl std::complex<double>

namespace Nbuk{


    struct Eigen
    {
        double mValue;
        arma::cx_vec mVector;

        Eigen(cpl nValue, arma::cx_vec nVector) :
            mValue{ nValue.real() }
          , mVector{ nVector }
        {

        }
    };

    enum class MagnetizationSelector
    {
        All, OneSelected
    };

    struct Magnetization
    {
        double mValue = 0.0;
        MagnetizationSelector mSelector = MagnetizationSelector::All;

        void operator = (double nValue)
        {
            mValue = nValue;
            mSelector = MagnetizationSelector::OneSelected;
        }

        void operator = (MagnetizationSelector nSelector)
        {
            mSelector = nSelector;
        }
    };


    struct ExperimentConfig
    {
        double mDelta = 1.0;
        double mJ = 1.0;
        uint mNumberOfNodes = 4;
        Magnetization mMagnetization;
        double mTemperature = 1.0;
        std::string mPathToWorkspace;
        bool mPBC;
        double mQ = 1.0;
        double h = 0.0;
    };

    cpl operator *(arma::cx_rowvec v1, arma::cx_vec v2);

    double mabs(cpl nValue);
    void mnormalise(std::vector<cpl> &vec);
    arma::cx_vec mnormalise(arma::cx_vec vec);
    std::string toBin(uint nValue, uint nLength);

    arma::cx_mat eigensToMatrix(std::vector<Eigen> nEigens);
    uint mrank(const arma::cx_mat &mat);
    std::vector<Eigen> exactDiagonalization(const arma::cx_mat &mat);

}
#endif // BASICMATH

