#include "basicmath.h"

#include <cassert>

namespace Nbuk{

    double mabs(cpl nValue)
    {
        return sqrt(pow(nValue.real(), 2) + pow(nValue.imag(), 2));
    }

    uint mrank(const arma::cx_mat &mat)
    {
        return (uint)sqrt( mat.size() );
    }

    void mnormalise(std::vector<cpl> &vec)
    {
        double lSum = 0.0;
        for(auto& val : vec){
            lSum += pow(val.real(), 2) + pow(val.imag(), 2);
        }
        for(auto& val : vec){
            val/=sqrt(lSum);
        }
    }

    arma::cx_vec mnormalise(arma::cx_vec vec)
    {
        return vec/sqrt( as_scalar(vec.t()*vec));
    }


    std::string toBin(uint nValue, uint nLength)
    {
        std::string oData;

        for(uint i=0; i<nLength; i++){
            if((nValue >> (nLength-1-i))%2){ oData+="1"; }
            else{ oData +="0"; }
        }
        return oData;
    }

    arma::cx_mat eigensToMatrix(std::vector<Eigen> nEigens)
    {
        arma::cx_mat oMat(nEigens[0].mVector.size(), nEigens.size());

        for(uint i=0; i<nEigens.size(); i++){
            oMat.col(i) = nEigens[i].mVector;
        }
        return oMat;
    }

    std::vector<Eigen> exactDiagonalization(const arma::cx_mat &mat)
    {
        arma::cx_mat tEignenvectors;
        arma::vec tEigenvalues;

        arma::eig_sym(tEigenvalues, tEignenvectors, mat);

        std::vector<Eigen> oEigens;
        for(uint i=0; i<tEigenvalues.size(); i++){
            oEigens.push_back( Eigen{tEigenvalues[i], tEignenvectors.col(i)} );
        }
        return oEigens;
    }
}
