#include "lanczos.h"

#include <cassert>

namespace Nbuk{

    arma::cx_vec randomVector(uint nSize)
    {
        arma::cx_vec startVec = arma::randn<arma::cx_vec>(nSize);
        startVec += arma::randn<arma::cx_vec>(nSize);
        return normalise(startVec);
    }

//    arma::cx_vec randomVector(uint nSize)
//    {
//        arma::cx_vec startVec = arma::cx_vec{nSize, arma::fill::ones};
//        startVec *= im;
//        startVec += arma::cx_vec{nSize, arma::fill::ones};
//        startVec += arma::randu<arma::cx_vec>(nSize);
//        startVec += arma::randn<arma::cx_vec>(nSize);
//        startVec += arma::randg<arma::cx_vec>(nSize);
//        startVec += arma::randg<arma::cx_vec>(nSize);
//        startVec += arma::randg<arma::cx_vec>(nSize);
//        startVec += arma::randu<arma::cx_vec>(nSize);
//        startVec += arma::randn<arma::cx_vec>(nSize);
//        startVec += arma::randg<arma::cx_vec>(nSize);
//        startVec += arma::randg<arma::cx_vec>(nSize);
//        startVec += arma::randg<arma::cx_vec>(nSize);
//        startVec += arma::randu<arma::cx_vec>(nSize);
//        startVec += arma::randn<arma::cx_vec>(nSize);
//        startVec += arma::randg<arma::cx_vec>(nSize);
//        startVec += arma::randg<arma::cx_vec>(nSize);
//        startVec += arma::randg<arma::cx_vec>(nSize);


//        startVec[0] *= 0.1739751;
//        startVec[nSize/2] *= 1.3328943;
//        startVec[nSize-1] *= 0.9138211;

//        return normalise(startVec);
//    }

    arma::cx_mat prepareLanczosMatrix(arma::cx_vec &a, arma::cx_vec &b)
    {
        uint M = a.size();
        arma::cx_mat lMatrix{ M, M, arma::fill::zeros };
        for(uint i=0; i<M-1; i++){
            lMatrix(i,i) = a[i];
            lMatrix(i,i+1) = b[i+1];
            lMatrix(i+1,i) = b[i+1];
        }
        lMatrix(M-1,M-1) = a[M-1];

        return lMatrix;
    }

    void prepareLanczosVectors(arma::cx_vec &a,
                               arma::cx_vec &b,
                               const arma::cx_mat &matrix,
                               arma::cx_vec startVector,
                               uint M,
                               arma::cx_mat &V)
    {
        a.resize(M); a.zeros(); b.resize(M); b.zeros();
        V.resize(mrank(matrix), M);

        arma::cx_vec PsiIm1{ mrank(matrix), arma::fill::zeros};
        arma::cx_vec PsiI = normalise(startVector);
        arma::cx_vec tmp2{ mrank(matrix), arma::fill::zeros};

        V.col(0) = PsiI;

        for(uint i=0; i<M-1; i++){
            a[i] = as_scalar(PsiI.t()*matrix*PsiI);
            b[i] = as_scalar(PsiIm1.t()*matrix*PsiI);
            tmp2 = matrix*PsiI - a[i]*PsiI-b[i]*PsiIm1;
            b[i+1] = norm(tmp2);

            PsiIm1 = PsiI;
            PsiI = tmp2/b[i+1];
            V.col(i+1) = PsiI;
        }
        a[M-1] = as_scalar(PsiI.t()*matrix*PsiI);
    }

    void prepareLanczosVectors(arma::cx_vec &a,
                               arma::cx_vec &b,
                               const arma::cx_mat &mat,
                               arma::cx_vec startVector,
                               uint M)
    {
        a.resize(M); a.zeros(); b.resize(M); b.zeros();

        arma::cx_vec PsiIm1{ mrank(mat), arma::fill::zeros};
        arma::cx_vec PsiI = normalise(startVector);
        arma::cx_vec tmp2{ mrank(mat), arma::fill::zeros};

        for(uint i=0; i<M-1; i++){
            a[i] = as_scalar(PsiI.t()*mat*PsiI);
            b[i] = as_scalar(PsiIm1.t()*mat*PsiI);
            tmp2 = mat*PsiI - a[i]*PsiI-b[i]*PsiIm1;
            b[i+1] = norm(tmp2);

            PsiIm1 = PsiI;
            PsiI = tmp2/b[i+1];
        }
        a[M-1] = as_scalar(PsiI.t()*mat*PsiI);
    }


    arma::cx_vec eigenstateLanczos(const arma::cx_mat &mat, arma::cx_vec startVector, arma::cx_vec lanczosVec, uint M)
    {
        cpl a=0.0, b=0.0;

        arma::cx_vec PsiI = normalise(startVector);
        arma::cx_vec PsiIm1{ mrank(mat), arma::fill::zeros};
        arma::cx_vec tmp2{ mrank(mat), arma::fill::zeros};

        arma::cx_vec oVec = lanczosVec[0]*PsiI;

        for(uint i=0; i<M-1; i++){
            a = as_scalar(PsiI.t()*mat*PsiI);
            b = as_scalar(PsiIm1.t()*mat*PsiI);
            tmp2 = mat*PsiI - a*PsiI- b*PsiIm1;
            b = norm(tmp2,2);

            PsiIm1 = PsiI;
            PsiI = tmp2/b;
            oVec += lanczosVec[i+1]*PsiI;
        }

        return -normalise(oVec);
    }


    std::vector<Eigen> lanczos(const arma::cx_mat &mat, uint M, arma::cx_mat &V)
    {
        return lanczos(mat, randomVector(mrank(mat)), M, V);
    }

    std::vector<Eigen> lanczos(const arma::cx_mat &mat, arma::cx_vec startVector, uint M, arma::cx_mat &V)
    {
        if(M>mrank(mat)){ M = mrank(mat); }
        arma::cx_vec a, b;

        prepareLanczosVectors(a,b,mat,startVector,M, V);
        arma::cx_mat T = prepareLanczosMatrix(a,b);

        return exactDiagonalization(T);
    }

    std::vector<Eigen> lanczos(const arma::cx_mat &mat, arma::cx_vec startVector, uint M)
    {
        if(M>mrank(mat)){ M = mrank(mat); }
        arma::cx_vec a, b;

        prepareLanczosVectors(a,b,mat,startVector,M);
        arma::cx_mat T = prepareLanczosMatrix(a,b);

        return exactDiagonalization(T);
    }

    std::vector<Eigen> lanczos(const arma::cx_mat &mat, uint M)
    {
        return lanczos(mat, randomVector(mrank(mat)), M);
    }

}
