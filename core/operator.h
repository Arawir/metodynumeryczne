#ifndef OPERATOR
#define OPERATOR

#include "base.h"

namespace Nbuk{
    class Operator
    {
    public:
        Base& mBase;
        arma::cx_mat mMatrix;
        std::vector<Eigen> mEigens;
    public:
        Operator(Base& nBase) :
            mBase{ nBase }
          , mMatrix{ }
        {

        }
        virtual ~Operator() = default;

        arma::cx_vec eigenvector(uint n)
        {
            return mEigens[n].mVector;
        }

        double eignevalue(uint n)
        {
            return mEigens[n].mValue;
        }

        virtual void regenerate()
        {
            if(sqrt(mMatrix.size()) != mBase.size()){
                prepareBase();
            }
            mMatrix.zeros();
            for(uint n=0; n<mBase.size(); n++){
                addState(mBase.state(n));
            }
            //assert(mMatrix.is_hermitian());
        }

        virtual void diagonalize()
        {
            mEigens.clear();
            arma::cx_mat tEignenvectors;
            arma::vec tEigenvalues;

            arma::eig_sym(tEigenvalues, tEignenvectors, mMatrix);

            for(uint i=0; i<tEigenvalues.size(); i++){
                mEigens.push_back( Eigen{tEigenvalues[i], tEignenvectors.col(i)} );
            }
        }

        virtual void regenerateAndDiagonalize()
        {
            regenerate();
            diagonalize();
        }

        void writeMatrixOnlyRe()
        {
            for(uint i=0; i<sqrt(mMatrix.size()); i++){
                for(uint j=0; j<sqrt(mMatrix.size()); j++){
                    if(mMatrix(i,j).real() >= 0.0) output << " ";
                    output << mMatrix(i,j).real() << "   ";
                }
                output << std::endl;
            }
        }

        arma::cx_mat &matrix()
        {
            return mMatrix;
        }

    protected:
        void prepareBase()
        {
            mMatrix.resize(mBase.size(), mBase.size());
            mMatrix.zeros();
        }

        virtual void addState(const BaseState nBaseState)
        {
            //EMPTY
        }
    };
}

#endif // OPERATOR

