#include "dataset.h"

namespace Nbuk{

    std::ostream& operator<<(std::ostream& os, Dataset& dataset)
    {
        for(auto& param : dataset.parametersCpl){
            os << param->name() << " ";
        }
        os << dataset.dataname() << std::endl;

        dataset.goToFirstField();

        while(dataset.fieldExists()){
            for(auto& param : dataset.parametersCpl){
                if(dataset.paramWasChanged(param->name()) && (param !=dataset.parametersCpl.back())){
                    os << std::endl;
                }
                os << dataset.valueOfParam(param->name()).real() << " "; //!!!!REAL
            }
            os << dataset.field().real() << std::endl;
            dataset.goToNextField();
        }

    }

}
