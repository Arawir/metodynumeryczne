#ifndef BASICHAMILTONIAN
#define BASICHAMILTONIAN

#include "operator.h"

namespace Nbuk{

    class BasicHamiltonian : public Operator
    {
    public:
        ExperimentConfig& mConfig;

        BasicHamiltonian(ExperimentConfig& nConfig, Base& nBase) :
            Operator{ nBase }
          , mConfig{ nConfig }
        {

        }
        ~BasicHamiltonian() = default;


        double energy(uint n)
        {
            return mEigens[n].mValue;
        }

        double maxEnergy()
        {
            double oEnergy = mEigens[0].mValue;
            for(auto& eigen : mEigens){
                if(eigen.mValue > oEnergy){
                    oEnergy = eigen.mValue;
                }
            }
            return oEnergy;
        }

        double minEnergy()
        {
            double oEnergy = mEigens[0].mValue;
            for(auto& eigen : mEigens){
                if(eigen.mValue < oEnergy){
                    oEnergy = eigen.mValue;
                }
            }
            return oEnergy;
        }

        double energyGap()
        {
            for(auto& eigen : mEigens){
                if(eigen.mValue != mEigens.at(0).mValue){
                    return eigen.mValue - mEigens.at(0).mValue;
                }
            }

            return 0.0;
        }

        double energyVariation()
        {
            double lAvEnergy = 0.0;
            double lAvEnergySquare = 0.0;

            for(uint n=0; n<mEigens.size(); n++){
                double lEn = mEigens.at(n).mValue;
                lAvEnergy += probabilityOfState(n)*lEn;
                lAvEnergySquare += probabilityOfState(n)*lEn*lEn;
            }
            return lAvEnergySquare - lAvEnergy*lAvEnergy;
        }

        double expectedEnergy()
        {
            double lZ = partitionFunction();
            double oEnergy = 0.0;

            for(auto& eigen : mEigens){
                oEnergy += eigen.mValue*exp(-eigen.mValue/mConfig.mTemperature) / lZ;
            }
            return oEnergy;
        }

        double partitionFunction()
        {
            double oZ = 0.0;

            for(auto& eigen : mEigens){
                oZ += exp(-eigen.mValue/mConfig.mTemperature);
            }
            return oZ;
        }

        double probabilityOfState(uint n)
        {
            double lZ = partitionFunction();
            if(mConfig.mTemperature < 0.001){
                if(energy(n) == minEnergy()){ return 1.0; }
                return 0.0;
            }

            if(mConfig.mTemperature > 1000.0){
                return 1.0/mBase.size();
            }

            return exp(-energy(n)/mConfig.mTemperature) / lZ;
        }

        double probabilityOfState(const Eigen &nEigen)
        {
            double lZ = partitionFunction();
            if(mConfig.mTemperature < 0.001){
                if(nEigen.mValue == minEnergy()){ return 1.0; }
                return 0.0;
            }

            if(mConfig.mTemperature > 1000.0){
                return 1.0/mBase.size();
            }

            return exp(-nEigen.mValue/mConfig.mTemperature) / lZ;
        }
    };

}
#endif // BASICHAMILTONIAN

