#ifndef EXPERIMENT3
#define EXPERIMENT3

#include "../operators/hamiltonian.h"
#include "../operators/operator_sigmazq.h"
#include "../core/nbuk.h"

using namespace Nbuk;

namespace Experiment3
{
    //analityczna teoria liniowej odpowiedzi
    void ex1b()
    {
        ExperimentConfig config;
        config.mTemperature = 20000.0;
        config.mDelta = 1.0;
        config.mJ = 1.0;
        config.mMagnetization = 0.0;
        config.mNumberOfNodes = 12;
        config.mPBC = true;

        Base base{ config };
        base.generateBase();

        Hamiltonian hamiltonian{ config, base };

        SigmaZQ sigmaZQ{ base, 0.0 };

        Dataset dataset;
        dataset.addParam("anisothropy", {1.0});
        dataset.addParam("temperature", {0.0,1.0,5000.0});
        dataset.addParam("q", 0.0,2.0*M_PI,config.mNumberOfNodes+1);
        dataset.addParam("omega", -2.1*M_PI,2.1*M_PI,101);
        dataset.sort({"anisothropy","temperature","omega","q"});
        dataset.dataname() = "S(q,omega)";
        dataset.verbosity() = Verbosity::progress;
        dataset.generateDatabaseAndGoToFirstField();

        for(uint indexAn=0; indexAn<dataset.numberOfParameters("anisothropy"); indexAn++){
            config.mDelta = dataset.paramWithIndex("anisothropy", indexAn).real();
            hamiltonian.regenerateAndDiagonalize();

            for(uint indexQ=0; indexQ<dataset.numberOfParameters("q"); indexQ++){
                sigmaZQ.mQ = dataset.paramWithIndex("q",indexQ).real();
                sigmaZQ.regenerate();

                for(uint indexT=0; indexT<dataset.numberOfParameters("temperature"); indexT++){
                    config.mTemperature = dataset.paramWithIndex("temperature", indexT).real();

                    for(auto& N : hamiltonian.mEigens){
                        for(auto& M : hamiltonian.mEigens){
                            double bin = dataset.paramDelta("omega").real();
                            uint indexOmega = (uint)((M.mValue-N.mValue)/bin-bin/2.0)+(dataset.numberOfParameters("omega")+1)/2;
                            dataset.fieldI({indexAn,indexT,indexOmega,indexQ}) += hamiltonian.probabilityOfState(N)*pow(mabs(as_scalar(N.mVector.t()*sigmaZQ.matrix()*M.mVector)),2)/bin;
                        }
                    }
                }
            }
        }
        dataset.saveToFiles3D("../data3/ex1_");
    }

}

#endif // EXPERIMENT3

