#ifndef EXPERIMENT6
#define EXPERIMENT6

#include "../operators/hamiltonian.h"
#include "../operators/disturbedhamiltonian.h"
#include "../operators/operator_sigmazq.h"
#include "../operators/operator_sigmaz.h"

#include "../core/nbuk.h"

using namespace Nbuk;

//LANCZOS #2
namespace Experiment6
{
    //CIEPŁO WŁAŚCIWE METODA LANCZOSA
    void ex1()
    {
        ExperimentConfig config;
        config.mNumberOfNodes =12;
        config.mJ = 1.0;
        config.mDelta = 1.0;
        config.mMagnetization = 0.0;
        config.mPBC = false;
        config.mTemperature = 0.1;

        Base base{ config }; base.generateBase();

        Hamiltonian H{ config, base }; H.regenerate();

        Dataset data;
        data.addParam("R", {1,2,5,10});
        data.addParam("T", 0.0,4.0,101);
        data.dataname() = "Cv(T)";
        data.generateDatabaseAndGoToFirstField();


//        arma::cx_mat V;

        for(auto &R : data.vectorOf("R")){
            arma::cx_vec startVec= randomVector(base.size());
            auto eigensLanczos = lanczos(H.matrix(), startVec, 100);
//            auto eigensLanczos = lanczos(H.matrix(), startVec, 100,V);
//            arma::cx_mat L = eigensToMatrix(eigensLanczos);
//            arma::cx_mat tmp3 = (startVec.t()*V*L).t();
//            arma::cx_vec przekrycie = tmp3.col(0);

            for(auto &T : data.vectorOf("T") ){
                double Z=0.0, HAv=0.0, HSquareAv=0.0;
                config.mTemperature = T.real();
                double beta = 1.0/config.mTemperature;

                for(uint r=0; r<(uint)R.real(); r++){
                    for(uint k=0; k<100; k++){
                        double przekrycie2 = pow( mabs(eigensLanczos[k].mVector[0]), 2);
                        //double przekrycie2 = pow( mabs(przekrycie[k]), 2);
                        double E = eigensLanczos[k].mValue;
                        Z += exp(-beta*E)*przekrycie2;
                        HAv += exp(-beta*E)*E*przekrycie2;
                        HSquareAv += exp(-beta*E)*E*E*przekrycie2;
                    }
                }

                HAv /= Z; HSquareAv /= Z;
                data.field({R, T}) = (HSquareAv-pow(HAv,2))/((double)config.mNumberOfNodes*pow(config.mTemperature,2));
            }
        }
        data.saveToFiles2D("../data6/POPRWIONEex1_Cv_");
    }




    cpl ulamek(arma::cx_vec &a, arma::cx_vec &b, cpl z, uint i)
    {
        if(i+1 >= b.size()){ return z - a.at(i); }
        return z - a.at(i) - pow(b.at(i+1),2)/ulamek(a,b,z,i+1);
    }


    //lanczos S(q,w)
    void ex2()
    {
        ExperimentConfig config;
        config.mNumberOfNodes = 8;
        config.mJ = 1.0;
        config.mDelta = 1.0;
        config.mMagnetization = 0.0;
        config.mPBC = true;

        Base base{ config };
        base.generateBase();

        Hamiltonian H{ config, base };
        H.regenerate();

        SigmaZQ Szq{base, 1.0};

        Dataset data;
        data.addParam("q", 0.0, 2.0*M_PI, config.mNumberOfNodes+1);
        data.addParam("omega", 0.0, M_PI, 101);
        data.dataname() = "S(q,omega)";
        data.generateDatabaseAndGoToFirstField();
        data.verbosity() = Verbosity::progress;

        arma::cx_vec a, b;

        arma::cx_vec startVec = randomVector(base.size());

        auto lanczosEigens = lanczos(H.matrix(),startVec,30);

        for(auto& q : data.vectorOf("q")){
            if(abs(q)<0.001) continue;
            Szq.mQ = q.real();
            Szq.regenerate();
            arma::cx_vec baseState = Szq.matrix()*eigenstateLanczos(H.matrix(), startVec, lanczosEigens[0].mVector, 30);

            for(auto& w : data.vectorOf("omega")){
                prepareLanczosVectors(a,b,H.matrix(),baseState, 30);
                double E0 = lanczos(H.matrix(),30)[0].mValue;
                cpl z = w + 0.001*im + E0;

                data.field({q,w}) = -(as_scalar(baseState.t()*baseState/ulamek(a,b,z,0))).imag()/M_PI;
            }
        }
        data.saveToFile("../data6/ex2");
    }




}


#endif // EXPERIMENT6

