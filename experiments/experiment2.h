#ifndef EXPERIMENT2
#define EXPERIMENT2

#include "../operators/hamiltonian.h"
#include "../operators/operator_sigmaz.h"

#include "../core/nbuk.h"
#include <list>

using namespace Nbuk;

namespace Experiment2
{
    //srednia E w funkcji temperatury
    void ex1()
    {
        ExperimentConfig config;
        config.mMagnetization = 0.0;


        Base base{ config };

        Hamiltonian hamiltonian{ config, base };

        Dataset dataset;
        dataset.addParam("L", {8, 10, 12});
        dataset.addParam("Anisotropy", {0., 1., 2.});
        dataset.addParam("Temperature", 0.0, 4.0, 101);
        dataset.sort({"L","Anisotropy","Temperature"});
        dataset.dataname() = "<E>/L";
        dataset.verbosity() = Verbosity::quiet;
        dataset.generateDatabaseAndGoToFirstField();

        for(auto& L : dataset.vectorOf("L")){
            config.mNumberOfNodes = (uint)L.real();
            base.generateBase();

            for(auto& A : dataset.vectorOf("Anisotropy")){
                config.mDelta = A.real();
                hamiltonian.regenerateAndDiagonalize();

                for(auto& T : dataset.vectorOf("Temperature")){
                    config.mTemperature = T.real();
                    dataset.field({L,A,T}) = hamiltonian.expectedEnergy()/L.real();
                    //dataset.field({L,A,T}) = hamiltonian.expectedEnergy();
                }
            }
        }
        dataset.saveToFiles2D("../data2/ex1_PBC_");
    }

    /////////////////////////////////////////////

    void ex2()
    {
        ExperimentConfig lConfig;
        lConfig.mNumberOfNodes = 12;
        lConfig.mDelta = 0.0;
        lConfig.mMagnetization = 0.0;
        lConfig.mPBC = true;

        Base lBase{ lConfig };
        lBase.generateBase();

        Hamiltonian lHamiltonian{ lConfig, lBase };
        lHamiltonian.regenerate();
        lHamiltonian.diagonalize();

        std::cout << lHamiltonian.maxEnergy() << std::endl;
        std::cout << lHamiltonian.minEnergy() << std::endl;

        State lState{ lBase };
        lState.setToBin( 0b111111000000 );


        arma::cx_vec a = lState.vector();
        arma::cx_mat b = lHamiltonian.matrix();
        cpl asd = as_scalar(a.t()*b*a);
        std::cout << "E/L = " << as_scalar(a.t()*b*a)/(double)lConfig.mNumberOfNodes << std::endl;
//        std::fstream lFile{ "../data2/ex2", std::ios::out};

//        double lTime =0.0;

//        while(lTime<20.0){
//            cpl lEnergy = arma::as_scalar(lState.mVector.t()*lHamiltonian.mMatrix*lState.mVector);
//            if(lEnergy.imag()*lEnergy.imag() < -0.001){
//                output << "WARNING - complex energy!";
//            }
//            lFile << lTime << " " << lEnergy.real() << std::endl;

//            lState.timeEvolution(0.1, lHamiltonian);
//            lTime+=0.1;
//        }

//        lFile.close();
    }

    ////////////////////////////////////////////////
    void ex3a()
    {
        ExperimentConfig config;
        config.mMagnetization = 0.0;
        config.mNumberOfNodes = 12;

        Base base{ config };
        base.generateBase();

        Hamiltonian hamiltonian{ config, base };

        State state{ base };
        state.setToBin( 0b111111000000 );

        TmeEvOper U{ hamiltonian };
        U.mDeltaT = 0.1;

        std::vector<SigmaZ> sigmaZList;
        for(uint i=0; i<config.mNumberOfNodes; i++){
            sigmaZList.push_back( SigmaZ{base, i} );
            sigmaZList.back().regenerate();
        }

        Dataset data;
        data.addParam("Anisotropy", {0.,2.});
        data.addParam("Time", 0.,20.,201);
        data.addParam("Node", 0,11,12);
        data.dataname() = "Magnetization";
        data.sort({"Anisotropy", "Time", "Node"});
        data.verbosity() = Verbosity::progress;
        data.generateDatabaseAndGoToFirstField();

        for(auto &A : data.vectorOf("Anisotropy")){
            config.mDelta = A.real();
            hamiltonian.regenerateAndDiagonalize();
            state.setToBin( 0b111111000000 );
            U.regenerate();

            for(auto &t : data.vectorOf("Time")){
                for(auto &n : data.vectorOf("Node")){
                    data.field({A,t,n}) = state.t() * sigmaZList[(uint)n.real()] * state;
                }

                state = U*state;
            }
        }
        data.saveToFiles3D("../data2/ex3_");
    }




    void saveDataToFile(std::fstream& nFile, double nTime, std::list<SigmaZ>& nSigmaZ, State& nState)
    {
        uint i=0;
        for(auto& sigmaZ : nSigmaZ){
            cpl lMagnetizationAtNode = arma::as_scalar(nState.mVector.t()*sigmaZ.mMatrix*nState.mVector);
            if(lMagnetizationAtNode.imag()*lMagnetizationAtNode.imag() < -0.001){
                output << "WARNING - complex magnetization!";
            }
            nFile << i << " "
                  << nTime << " "
                  << lMagnetizationAtNode.real() << std::endl;
            i++;
        }
        nFile << std::endl;
    }

    void doEx3(ExperimentConfig& nConfig)
    {
        Base lBase{ nConfig };
        lBase.generateBase();

        Hamiltonian lHamiltonian{ nConfig, lBase };
        lHamiltonian.regenerate();
        lHamiltonian.diagonalize();

        std::list<SigmaZ> lSigmaZ;
        for(uint i=0; i<nConfig.mNumberOfNodes; i++){
            lSigmaZ.push_back( SigmaZ{lBase, i} );
            lSigmaZ.back().regenerate();
        }

        State lState{ lBase };

        uint lBin = 0;
        for(uint i=0; i<nConfig.mNumberOfNodes/2; i++){
            lBin+=1;
            lBin*=2;
        }
        for(uint i=nConfig.mNumberOfNodes/2; i<nConfig.mNumberOfNodes-1; i++){
            lBin*=2;
        }

        lState.setToBin( lBin );

        std::string lFileName = "../data2/ex2_" + std::to_string(nConfig.mNumberOfNodes)
                                                + std::to_string(nConfig.mDelta);

        std::fstream lFile{ lFileName, std::ios::out};

        double lTime =0.0;

        while(lTime<20.0){
            saveDataToFile(lFile, lTime, lSigmaZ, lState);
            lState.timeEvolution(0.1, lHamiltonian);
            lTime+=0.1;
        }

        lFile.close();
    }

    void ex3()
    {
        ExperimentConfig lConfig;
        lConfig.mMagnetization = 0.0;

        output << "UPDATE : Ex3_0" << std::endl;
        lConfig.mNumberOfNodes = 8;
        lConfig.mDelta = 0.0;
        doEx3(lConfig);

        output << "UPDATE : Ex3_1" << std::endl;
        lConfig.mNumberOfNodes = 8;
        lConfig.mDelta = 1.0;
        doEx3(lConfig);

        output << "UPDATE : Ex3_2" << std::endl;
        lConfig.mNumberOfNodes = 8;
        lConfig.mDelta = 2.0;
        doEx3(lConfig);

        output << "UPDATE : Ex3_0" << std::endl;
        lConfig.mNumberOfNodes = 14;
        lConfig.mDelta = 0.0;
        doEx3(lConfig);

        output << "UPDATE : Ex3_1" << std::endl;
        lConfig.mNumberOfNodes = 14;
        lConfig.mDelta = 1.0;
        doEx3(lConfig);

        output << "UPDATE : Ex3_2" << std::endl;
        lConfig.mNumberOfNodes = 14;
        lConfig.mDelta = 2.0;
        doEx3(lConfig);
    }
}


#endif // EXPERIMENT2

