#ifndef EXPERIMENT4_H
#define EXPERIMENT4_H

#include "../operators/hamiltonian.h"
#include "../operators/disturbedhamiltonian.h"
#include "../operators/operator_sigmazq.h"
#include "../operators/operator_sigmaz.h"
#include "../core/nbuk.h"

using namespace Nbuk;


namespace Experiment4
{
    //zaburzenie w hamiltonianie - teoria liniowej odpowiedzi numerycznie
    void ex1()
    {
        ExperimentConfig config;
        config.mTemperature = 0.0;
        config.mDelta = 1.0;
        config.mJ = 1.0;
        config.mMagnetization = 0.0;
        config.mNumberOfNodes = 8;
        config.mPBC = true;
        config.h = 10.0;

        Base base{ config }; base.generateBase();

        Hamiltonian H{ config, base };
        H.regenerateAndDiagonalize();

        DisturbedHamiltonian dH{ config, base };

        State state{ base };

        TmeEvOper U{ H };
        U.mDeltaT = 0.01;
        U.regenerate();

        SigmaZQ szq{base, config.mQ };

        Dataset data;
        data.dataname() = "S(q,w)";
        data.addParam("h", {0.01, 0.1, 1.0, 10.0});
        data.addParam("omega", 0.0,M_PI,101);
        data.addParam("q", 0.0,2.0*M_PI,101);
        data.sort({"h", "q", "omega"});
        data.verbosity() = Verbosity::progress;
        data.generateDatabaseAndGoToFirstField();

        for(auto& h : data.vectorOf("h")){
            config.h = h.real();

            for(auto& q : data.vectorOf("q")){
                config.mQ = q.real();
                dH.regenerateAndDiagonalize();
                state = dH.eigenvector(0);
                szq.mQ = q.real();
                szq.regenerate();

                for(double t=0.0; t<8.0*(double)config.mNumberOfNodes+U.mDeltaT/2.0; t+=U.mDeltaT){
                    cpl tS = state.t()*szq*state;
                    for(auto& omega : data.vectorOf("omega")){
                        data.field({h,q,omega}) += U.mDeltaT*exp(im*omega*t)*tS;
                    }
                    state = U*state;
                }
            }
        }
        data.saveToFiles3D("../data4/ex1_");
    }

}

#endif // EXPERIMENT4_H

