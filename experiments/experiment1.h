#ifndef EXPERIMENT1
#define EXPERIMENT1

#include "../core/nbuk.h"
#include "../operators/hamiltonian.h"

using namespace Nbuk;

namespace Experiment1
{
    void ex1()
    {
        ExperimentConfig config;
        config.mJ = 1.0;
        config.mDelta = 1.0;
        config.mMagnetization = MagnetizationSelector::All;
        config.mNumberOfNodes = 4;

        Base base{ config };
        base.generateBase();

        Hamiltonian hamiltonian{ config, base };
        hamiltonian.regenerateAndDiagonalize();

        std::fstream file("../data1/ex1", std::ios::out);
        file << "EigenValue <Psi|H|Psi>(real) <Psi|H|Psi>(imag)" << std::endl;

        for(auto& eigen : hamiltonian.mEigens){
            file << eigen.mValue << " "
                 << as_scalar(eigen.mVector.t() * hamiltonian * eigen.mVector).real() << " "
                 << as_scalar(eigen.mVector.t() * hamiltonian * eigen.mVector).imag() << " "
                 << std::endl;
        }
        file.close();
    }

    //Przerwa energetyczna
    void ex2()
    {
        ExperimentConfig config;
        config.mJ = 1.0;
        config.mMagnetization = 0.0;
        config.mNumberOfNodes = 2;
        config.mPBC = false;

        Base base{ config };

        Hamiltonian hamiltonian{ config, base };

        Dataset data;
        data.addParam("PBC", {0,1});
        data.addParam("Anisotropy", {0.5, 1.0, 2.0});
        data.addParam("L", {2,4,6,8,10,12,14});
        data.sort({"PBC","Anisotropy", "L"});
        data.dataname() = "Energy gap";
        data.generateDatabaseAndGoToFirstField();
        data.verbosity() = Verbosity::progress;

        for(auto &PBC : data.vectorOf("PBC")){
            config.mPBC = (bool)PBC.real();
            for(auto &L : data.vectorOf("L")){
                config.mNumberOfNodes = (uint)L.real();
                base.generateBase();

                for(auto &An : data.vectorOf("Anisotropy")){
                    config.mDelta = An.real();
                    hamiltonian.regenerateAndDiagonalize();
                    data.field({PBC,An,L}) = (hamiltonian.energy(1)-hamiltonian.energy(0))/L;
                    //data.field({PBC,An,L}) = (hamiltonian.energy(1)-hamiltonian.energy(0));
                }
            }
        }

        data.saveToFiles2D("../data1/ex2_");
    }


    void generateAndSaveDataseries(ExperimentConfig& nConfig, std::string nFileName);

    //Pojemność cieplna
    void ex3()
    {
        ExperimentConfig config;
        config.mJ = 1.0;
        //config.mMagnetization = MagnetizationSelector::All;
        config.mMagnetization = 0.0;
        config.mPBC = false;

        Base base{ config };
        Hamiltonian hamiltonian{ config, base };

        Dataset data;
        data.addParam("L", {4,8,12,16});
        data.addParam("Anisotropy", {0.5, 1.0, 2.0});
        data.addParam("Temperature", 0, 4.0, 101);
        data.dataname() = "Heat capacity (Cv)";
        data.sort({"L","Anisotropy", "Temperature"});
        data.generateDatabaseAndGoToFirstField();
        data.verbosity() = Verbosity::progress;

        for(auto &L : data.vectorOf("L")){
            config.mNumberOfNodes = (uint)L.real();
            base.generateBase();

            for(auto &An : data.vectorOf("Anisotropy")){
                config.mDelta = An.real();
                hamiltonian.regenerateAndDiagonalize();

                for(auto &T : data.vectorOf("Temperature")){
                    config.mTemperature = T.real();
                    data.field({L,An,T}) = hamiltonian.energyVariation()/(L*T*T);
                }
            }
        }

        data.saveToFiles2D("../data1/ex3_");
    }
}




#endif // EXPERIMENT1

