#ifndef EXPERIMENT5
#define EXPERIMENT5

#include <fstream>
#include "../operators/hamiltonian.h"
#include "../operators/disturbedhamiltonian.h"
#include "../operators/operator_sigmazq.h"
#include "../operators/operator_sigmaz.h"
#include "../core/nbuk.h"

using namespace Nbuk;

//LANCZOS #1
namespace Experiment5
{
    //lanczos_zbieżnośc
    void ex1()
    {
        ExperimentConfig config;
        config.mNumberOfNodes = 6;
        config.mJ = 1.0;
        config.mDelta = 1.0;
        config.mMagnetization = 0.0;
        config.mPBC = false;

        Base base{ config };
        base.generateBase();

        Hamiltonian H{ config, base };
        H.regenerate();

        std::fstream file{"../data5/ex1", std::ios::out};

        for(uint i=1; i<base.size()+2; i++){
            for(auto& eigen : lanczos(H.matrix(), i)){
                file << i << " "
                     << eigen.mValue << std::endl;
            }
        }
        file.close();

        file.open("../data5/ex1_exact", std::ios::out);
        file << 0 << " ";
        for(auto& eigen : exactDiagonalization(H.matrix())){
            file << eigen.mValue << " ";
        }
        file << std::endl << 50 << " ";
        for(auto& eigen : exactDiagonalization(H.matrix())){
            file << eigen.mValue << " ";
        }
        file.close();
    }

    //lanczos precyzja stan podstawowy
    void ex2()
    {
        ExperimentConfig config;
        config.mNumberOfNodes = 10;
        config.mJ = 1.0;
        config.mDelta = 1.0;
        config.mMagnetization = 0.0;
        config.mPBC = false;

        Base base{ config };
        base.generateBase();

        Hamiltonian H{ config, base };
        H.regenerateAndDiagonalize();

        Dataset dataset;
        dataset.addParam("step", 1, base.size(), base.size());
        dataset.dataname() = "|El-Ee|/Ee";
        dataset.generateDatabaseAndGoToFirstField();

        for(auto& step : dataset.vectorOf("step")){
            double Elanczos = lanczos(H.matrix(), (uint)step.real())[0].mValue;
            double Eexact = H.minEnergy();
            dataset.field({step}) = mabs((Elanczos-Eexact)/Eexact);
        }
        dataset.saveToFile("../data5/ex2", "scientific");
    }


}

#endif // EXPERIMENT5

