#ifndef DISTURBEDHAMILTONIAN
#define DISTURBEDHAMILTONIAN

#include "../core/basichamiltonian.h"

class DisturbedHamiltonian : public BasicHamiltonian
{
public:
    DisturbedHamiltonian(ExperimentConfig& nConfig, Base& nBase) :
        BasicHamiltonian{ nConfig, nBase }
    {

    }
    ~DisturbedHamiltonian() = default;

private:
    void addState(const BaseState nBaseState) override
    {
        for(uint n=0; n<nBaseState.mNumberOfNodes-1; n++){
            uint lSubstate = nBaseState.twoNodeSubstate(n,n+1);

            mMatrix(nBaseState.mNumber, nBaseState.mNumber) += potentialValueOfSubstate(lSubstate);
            if( (lSubstate == 0b01) || (lSubstate == 0b10) ){
                BaseState tState = nBaseState;
                tState.swapNodes(n,n+1);
                mMatrix(nBaseState.mNumber, tState.mNumber) += mConfig.mJ / 2.0;
            }
        }

        if(mConfig.mPBC==true){
            uint lSubstate = nBaseState.twoNodeSubstate(nBaseState.mNumberOfNodes-1,0);

            mMatrix(nBaseState.mNumber, nBaseState.mNumber) += potentialValueOfSubstate(lSubstate);
            if( (lSubstate == 0b01) || (lSubstate == 0b10) ){
                BaseState tState = nBaseState;
                tState.swapNodes(nBaseState.mNumberOfNodes-1,0);
                mMatrix(nBaseState.mNumber, tState.mNumber) += mConfig.mJ / 2.0;
            }
        }

        for(uint n=0; n<nBaseState.mNumberOfNodes; n++){
            uint lSubstate = nBaseState.oneNodeSubstate(n);

            if( lSubstate == 0b0 ){
                mMatrix(nBaseState.mNumber, nBaseState.mNumber) += -mConfig.h*0.5*cos(mConfig.mQ*(double)n);
            } else {
                mMatrix(nBaseState.mNumber, nBaseState.mNumber) += mConfig.h*0.5*cos(mConfig.mQ*(double)n);
            }
        }

    }

    double potentialValueOfSubstate(uint nTwoNodeSubstate)
    {
        if(nTwoNodeSubstate==0) return mConfig.mJ*mConfig.mDelta/4.0;
        if(nTwoNodeSubstate==1) return -mConfig.mJ*mConfig.mDelta/4.0;
        if(nTwoNodeSubstate==2) return -mConfig.mJ*mConfig.mDelta/4.0;
        if(nTwoNodeSubstate==3) return mConfig.mJ*mConfig.mDelta/4.0;
        assert("wrong substate!");
        return 0.0;
    }
};

#endif // DISTURBEDHAMILTONIAN

