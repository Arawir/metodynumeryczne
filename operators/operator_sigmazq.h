#ifndef OPERATOR_SIGMAZQ
#define OPERATOR_SIGMAZQ

#include "../core/operator.h"

class SigmaZQ : public Operator
{
public:
    double mQ;
public:
    SigmaZQ(Base& nBase, double nQ) :
        Operator{ nBase }
      , mQ{ nQ }
    {

    }

private:
    void addState(const BaseState nBaseState) override
    {
        for(uint n=0; n<nBaseState.mNumberOfNodes; n++){
            uint lSubstate = nBaseState.oneNodeSubstate(n);

            if( lSubstate == 0b0 ){
                mMatrix(nBaseState.mNumber, nBaseState.mNumber) += -0.5*exp(im*mQ*static_cast<double>(n));
            } else {
                mMatrix(nBaseState.mNumber, nBaseState.mNumber) += 0.5*exp(im*mQ*static_cast<double>(n));
            }
        }

    }
};

#endif // OPERATOR_SIGMAZQ

