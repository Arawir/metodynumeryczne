#ifndef OPERATOR_SIGMAZ
#define OPERATOR_SIGMAZ

#include "../core/operator.h"

class SigmaZ : public Operator
{
public:
    uint mNodeNumber;

public:
    SigmaZ(Base& nBase, uint nNodeNumber) :
        Operator{ nBase }
      , mNodeNumber{ nNodeNumber }
    {

    }

private:
    void addState(const BaseState nBaseState) override
    {
        uint lSubstate = nBaseState.oneNodeSubstate(mNodeNumber);
        if(lSubstate==0b0){
            mMatrix(nBaseState.mNumber, nBaseState.mNumber) += -0.5;
        } else {
            mMatrix(nBaseState.mNumber, nBaseState.mNumber) += 0.5;
        }
    }
};

#endif // OPERATOR_SIGMAZ

